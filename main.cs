using System;
using System.Collections.Generic;
using System.Linq;

class Program {
  public static void Main (string[] args) {
    string str = "thequickbrownfoxjumpsoverthelazydog";
    Dictionary<char, int> frequencies = new Dictionary<char, int>();
    
    foreach (var chr in str) 
      IncAt(chr, frequencies);
    
    var topThree = frequencies.Keys
      .Zip(frequencies.Values, (character, count) => (character, count))
      .OrderByDescending(pair => pair.count)
      .Take(3)
      .ToList();
    
    foreach (var top in topThree)
      Console.WriteLine($"{top.character} : {top.count}");
  }

  private static void IncAt(char chr, Dictionary<char, int> dict) {
    if (dict.ContainsKey(chr)){
      dict[chr] += 1;
    } else {
      dict[chr] = 1;
    }
  }
}

// Outputs:
//  o : 4
//  e : 3
//  t : 2
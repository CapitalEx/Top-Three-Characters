| phrase frequency ranking topThree |

phrase    := 'thequickbrownfoxjumpsoverthelazydog'.
frequency := phrase 
	inject: (Dictionary new) 
	into: [:currentCount :letter | 
		currentCount at: letter update: [:count | count + 1 ] initial: [ 1 ]
	].
ranking   := frequency associations sorted: [ :a :b | b value <= a value ].
topThree  := ranking first: 3.

topThree do: [ :letter | Transcript show: letter ; cr  ]
import java.util.HashMap;
import java.util.stream.Collectors;

class Main {
  public static void main(String[] args) {
    var str = "thequickbrownfoxjumpsoverthelazydog";
    var frequencies = new HashMap<Character, Integer>();

    for (var chr : str.toCharArray())
      incAt(chr, frequencies);

    var topThree = frequencies.entrySet()
      .stream()
      .sorted((a, b) -> b.getValue().compareTo(a.getValue()))
      .limit(3)
      .collect(Collectors.toList());

    for (var entry : topThree) 
      System.out.println(entry.getValue() + " : " + entry.getKey());
  }

  private static void incAt(char chr, HashMap<Character, Integer> map) {
    if (map.containsKey(chr)) {
      map.put(chr, map.get(chr) + 1);
    } else {
      map.put(chr, 1);
    }
  }
}

// Outputs:
// 4 : o
// 3 : e
// 2 : h
:- module main.
:- interface.

:- import_module io.

:- pred main(io::di, io::uo) is det.


:- implementation.

:- import_module map.
:- import_module char.
:- import_module int.
:- import_module list.
:- import_module string.
:- import_module pair.

main(!IO) :-
    Str = "thequickbrownfoxjumpsoverthelazydog",
    map.init(Frequencies0),

    foldl(inc_at, Str, Frequencies0, Frequencies),
    to_assoc_list(Frequencies, Pairing),
    sort(by_count, Pairing, SortedPairing),
    reverse(SortedPairing, TopPairing),
    
    take_upto(3, TopPairing, Top),
    write_pairs(Top, !IO).


:- pred by_count(pair(char, int)::in, pair(char, int)::in, comparison_result::uo) is det.
by_count(A, B, Res) :-
    snd(A) = with_type(CountA, int),
    snd(B) = with_type(CountB, int),
    compare(Res, CountA, CountB).

:- pred inc_at(char::in, map(char, int)::in, map(char, int)::out) is det.
inc_at(Key, !Frequencies) :-
    ( search(!.Frequencies, Key, Count) ->
        map.det_update(Key, Count + 1, !Frequencies)
    ;
        map.det_insert(Key, 1, !Frequencies)
    ).

:- pred write_pairs(list(pair(char, int))::in, io::di, io::uo) is det.
write_pairs([], !IO).
write_pairs([Pair | Pairs], !IO) :-
    fst(Pair) = Char,
    snd(Pair) = Count,
    write_frequency(Char, Count, !IO),
    write_pairs(Pairs, !IO).

:- pred write_frequency(char::in, int::in, io::di, io::uo) is det.
write_frequency(Char, Count, !IO) :-
    io.write_char(Char, !IO),
    io.write_string(" : ", !IO),
    io.write_int(Count, !IO),
    io.write_string("\n", !IO).

% Outputs
%   o : 4
%   e : 3
%   u : 2

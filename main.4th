: 3dup         ( a b c      -- a b c a b c )  dup 2over rot ;
: zero         ( addr       --             )  0 swap ! ;
: clear-cells  ( addr n     --             )  0 do dup i cells + zero loop drop ;
: clear-chars  ( addr n     --             )  0 do dup i chars + zero loop drop ;
: inc!         ( addr       --             )  1 swap +! ;
: get-char     ( addr n     --        char )  chars + c@ ;
: count-letter ( addr char  --             )  chars + inc! ;

\ Each cell holds 2 values:
\    - char
\    - count
create 1st 2 cells allot  0 0 1st 2!
create 2nd 2 cells allot  0 0 2nd 2!
create 3rd 2 cells allot  0 0 3rd 2!
: char>>     ( addr        -- char ) @ ;
: count>>    ( addr        --    n ) 1 cells + @ ;
: >>char     ( n addr      -- addr ) tuck ! ;
: >>count    ( n addr      -- addr ) tuck 1 cells + ! ;
: exchange   ( addr addr   --      ) swap 2@ rot 2! ;
: set-nth    ( char m addr --      ) >>count >>char drop ;

: ?update-3rd ( char m -- )
  dup 3rd count>> > if 3rd set-nth else 2drop then ;

: ?update-2nd ( char m -- )
  dup 2nd count>> > if
    2nd 3rd exchange
    2nd set-nth
  else ?update-3rd then ;

: ?update-1st ( char m -- )
  dup 1st count>> > if
    2nd 3rd exchange
    1st 2nd exchange
    1st set-nth
  else ?update-2nd then ;

create visited   256 chars allot   visited 256 clear-chars
: ?visited           ( char  --    ? ) visited swap chars + c@ ;
: letter-visited     ( char  -- char ) dup visited swap chars + 1 swap c! ; 
: reject             ( a b c --      ) 2drop drop ;

create frequency 256 chars allot frequency 256 clear-chars
: frequency>> ( addr n      -- m ) chars + c@ ;

: letter-frequency ( frequency-addr string-addr n -- )
  0 do 2dup i get-char count-letter loop 2drop ;

: rank ( frequency-addr char --  )
  tuck dup ?visited if
    reject
  else
    letter-visited frequency>> ?update-1st
  then ;

: top-three ( frequency-addr string-addr n -- )
  0 do 2dup i get-char rank loop 2drop ;


frequency s" thequickbrownfoxjumpsoverthelazydog" 3dup letter-frequency top-three

1st dup count>> . s" : " type char>> emit cr
2nd dup count>> . s" : " type char>> emit cr
3rd dup count>> . s" : " type char>> emit cr

\ Outputs: 
\   4 : o
\   3 : e
\   2 : t
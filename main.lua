local function inc_at ( at, table )
    table[at] = table[at] and table[at] + 1 or 1
end

local function letter_frequency ( string )
    local t = {}
    for char in string:gmatch(".") do
        inc_at(char, t)
    end
    return t
end

local frequencies = letter_frequency "thequickbrownfoxjumpsoverthelazydog"
local pairings = {}
for k, v in pairs(frequencies) do
    pairings[#pairings + 1] = { k, v }
end

table.sort(pairings, function(a, b) return a[2] > b[2] end)
print(pairings[1][1], pairings[1][2])
print(pairings[2][1], pairings[2][2])
print(pairings[3][1], pairings[3][2])

--[[
  Outpus:
    o   4
    e   3
    r   2
]]
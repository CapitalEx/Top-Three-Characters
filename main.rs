use std::collections::HashMap;

fn inc_at(mut hash_map: HashMap<char, u32>, chr: char) -> HashMap<char, u32> {
    match hash_map.get(&chr) {
        Some(&val) => hash_map.insert(chr, val + 1),
        None => hash_map.insert(chr, 1),
    };
    hash_map
}

fn letter_frequency(string: &str) -> Vec<(char, u32)> {
    return string
        .chars()
        .fold(HashMap::new(), inc_at)
        .into_iter()
        .collect::<Vec<(char, u32)>>();
}

fn main() {
    let mut pairs = letter_frequency("thequickbrownfoxjumpsoverthelazydog");
    pairs.sort_by(|a, b| b.1.cmp(&a.1));
    print!("{:?}\n", vec![pairs[0], pairs[1], pairs[2]]);
}

// Outputs:
//    [('o', 4), ('e', 3), ('t', 2)]
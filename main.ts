function inc_at(
  map: { [key: string]: number },
  char: string,
): { [key: string]: number } {
  return map[char] !== undefined
    ? { ...map, [char]: map[char] + 1 }
    : { ...map, [char]: 1 };
}

function letter_frequency(s: string): { [key: string]: number } {
  return s.split("")
    .reduce(inc_at, {});
}

const frequencies = letter_frequency("thequickbrownfoxjumpsoverthelazydog");
const first3 = Object.entries(frequencies)
  .sort((a, b) => b[1] - a[1])
  .slice(0, 3);
console.log(first3);

// Outputs:
//    [ [ "o", 4 ], [ "e", 3 ], [ "t", 2 ] ]
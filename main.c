#include <stdio.h>

typedef struct {
  char letter;
  int count;
} frequency;

int main(void) {
  const char string[]     = "thequickbrownfoxjumpsoverthelazydog";
        char letters[256] = {};

  for (int i = 0; string[i] != '\0'; i++)
    letters[string[i]] += 1;

  frequency top_three[] = {
      {-1, -1},
      {-1, -1},
      {-1, -1},
  };

  for (int i = 0; string[i] != '\0'; i++) {
    const int letter        = string[i];
    const int current_count = letters[letter];
          int rank          = 3;

    if (current_count == 0) continue;

    letters[letter] = 0;

    if      (top_three[0].count < current_count) rank = 0;
    else if (top_three[1].count < current_count) rank = 1;
    else if (top_three[2].count < current_count) rank = 2;

    for (int i = 2; i > rank; i--)
      top_three[i] = top_three[i - 1];
    
    top_three[rank] = (frequency){letter, current_count};
  }

  for (int i = 0; i < 3; i++)
    printf("%c\t%d\n", top_three[i].letter, top_three[i].count);

  return 0;
}

// Outputs:
//	o   4
//	e   3
//	t   2
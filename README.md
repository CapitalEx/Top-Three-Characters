# Rosseta Coding for Fun

This repo contains the code from 14 different languages:

- Forth
- BQN
- C
- Clojure
- C++
- C#
- Factor
- Haskell
- Java
- Lua
- Mercury
- Rust
- TypeScript
- SmallTalk

Each language is my best attempt to find the three most common
letters in a string of text while maintaining as much of a
functional/declarative style as possible. Additionally, each
repo makes use of the newest features available in each language. 

The letter output in the event of a tie does not matter. 


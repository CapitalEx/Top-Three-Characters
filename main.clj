(defn inc-at [m x]
  (update m x
          #(if (nil? %) 1
               (+ % 1))))

(defn top-3 [m]
  (take 3
        (sort-by val #(compare %2 %1) m)))

(println
 (top-3
  (reduce inc-at {}
          (seq "thequickbrownfoxjumpsoverthelazydog"))))

;; Outputs:
;;   ([o 4] [e 3] [h 2])
import Data.Map (Map, insertWith, fromList, toList, empty)
import Data.List (sortBy)

incAt :: Char -> Map Char Int -> Map Char Int
incAt char = insertWith (+) char 1

letterFrequencies :: [Char] -> Map Char Int
letterFrequencies = foldr incAt empty

main = do
  print $ take 3 
        $ sortBy (\a b -> compare (snd b) (snd a)) 
        $ toList 
        $ letterFrequencies "thequickbrownfoxjumpsoverthelazydog"


-- Outputs:
--    [('o',4),('e',3),('h',2)]
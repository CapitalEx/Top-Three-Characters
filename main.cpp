#include <algorithm>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

auto letter_frequency(unordered_map<char, int> &map, char c) {
  map[c] = map.find(c) != map.end() ? map[c] + 1 : 1;
  return map;
}

int main() {
  string s("thequickbrownfoxjumpsoverthelazydog");
  auto frequencies = 
    accumulate( next(s.begin()), s.end(), 
                unordered_map<char, int>{},
                letter_frequency);

  vector<std::pair<char, int>> pairings(frequencies.begin(), frequencies.end());
  sort(pairings.begin(), pairings.end(), [](auto a, auto b) { return b.second < a.second; });

  auto first = pairings[0];
  auto second = pairings[1];
  auto thrid = pairings[2];

  cout << first.first  << "\t" << first.second  << endl
       << second.first << "\t" << second.second << endl
       << thrid.first  << "\t" << thrid.second  << endl;
}

// Outputs:
//  o   4
//  e   3
//  r   2
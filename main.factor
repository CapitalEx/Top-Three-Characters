! Copyright (C) 2022 Capital.
! See http://factorcode.org/license.txt for BSD license.
USING: arrays assocs kernel prettyprint sequences sorting
strings ;
IN: top-three

: letter-frequency ( string -- hashmap )
    H{ } clone [ 1string over inc-at ] reduce ;

: top-3 ( hashmap -- 1st 2nd 3rd )
    sort-values reverse first3 ;

"thequickbrownfoxjumpsoverthelazydog" letter-frequency top-3 3array .

! Outputs:
!   { { "o" 4 } { "e" 3 } { "u" 2 } }
